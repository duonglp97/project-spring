package com.example.demo.apicontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Contact;
import com.example.demo.service.ContactService;
import com.example.demo.service.IContact;

@RestController
@RequestMapping("/api/contact")
public class ContactApiController {
	
	@Autowired
	private IContact contactService;

	@GetMapping
	public Iterable<Contact> getAllContacts() {
		return contactService.findAll();
	}

	@PostMapping
	public String addContact(@RequestBody Contact contact) {
		Contact addContact = new Contact();
		addContact.setName(contact.getName());
		addContact.setEmail(contact.getEmail());
		addContact.setPhone(contact.getPhone());
		addContact.setLink(contact.getLink());
		contactService.save(addContact);

		return "Them thanh cong";
	}
	
	@DeleteMapping(path = "/{id}")
	public String deleteContact(@PathVariable int id) {
		contactService.delete(id);
		return "xoa thanh cong";
	}
	
	@PutMapping(path = "/{id}")
	public String updateContact(@PathVariable int id, @RequestBody Contact contact) {
		
		if (contactService.findOne(id).isPresent()) {
			Contact updateContact = contact;
			updateContact.setId(id);
			contactService.save(updateContact);
			return "update thanh cong";
		}
		return "khong tim thay";
	}
}
