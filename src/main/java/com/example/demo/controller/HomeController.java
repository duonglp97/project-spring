package com.example.demo.controller;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Contact;
import com.example.demo.entity.User;

import com.example.demo.service.IContact;
import com.example.demo.service.IUser;





@Controller

public class HomeController {
	@Autowired
	private IUser userService;
	
	@Autowired
	private IContact contactService;
	
	@Value("${UPLOAD_FOLDER}")
	private String fileDirectly;
	
	public List<Contact> fakeData(){
		List<Contact> contacts = new ArrayList<Contact>();
		for (int i = 0; i < 10; i++) {
			contacts.add(new Contact(i ,"Duong" , "duonglp97@gmail.com" , 946235025, "bsnl2.jpg"));
		}
		return contacts;
	}
	
	@GetMapping(value = "/login")
	public String getLogin(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "login";
	}
	
//	@PostMapping(value = "/login")
//	public String login(Model model , @ModelAttribute("user") User user) {
//		User checkedUser = userService.findByNameAndPassword(user.getName(), user.getPass());
//		if (checkedUser != null) {
//			return "redirect:/contact";
//		}
//		
//		return "login";
//	}
	
	@GetMapping(value = "/update-contact")
	public String addContact(Model model) {
		return "add-contact";
	}
	
	@GetMapping(value = {"/", "/index"})
	public String list(Model model) {
		if(contactService.count()<10) {
			
			for (Iterator iterator = fakeData().iterator(); iterator.hasNext();) {
				Contact contact = (Contact) iterator.next();
				contactService.save(contact);
			}
		}
		model.addAttribute("contacts", contactService.findAll());
		return "index";
	}
	
	@GetMapping(value = "/contact/search")
	public String search(Model model, @RequestParam("searchString") String searchString){
		if(searchString.isEmpty()) {
			return "redirect:/index";
		}
		model.addAttribute("contacts", contactService.search(searchString));
		return "index";
	}
	
	
	@GetMapping(value = "/contact/add")
	public String add(Model model) {
		Contact contact = new Contact();
		model.addAttribute("contact", contact);
		return "add-contact";
	}
	
	
	@GetMapping(value = "/contact/{id}/delete")
	public String delete(@PathVariable int id, RedirectAttributes redirect) {
		contactService.delete(id);
		redirect.addFlashAttribute("SuccessMessage", "Delete Contact Successfully");
		return "redirect:/index";
	}
	
	
	@PostMapping(value = "/contact/save")
	public String save(@Valid Contact contact,@RequestParam("image") MultipartFile img, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "add-contact";
		}
		
		try {
			byte[] bytes = img.getBytes();
			Path path = Paths.get(fileDirectly + img.getOriginalFilename());
			Files.write(path, bytes);
//			System.out.println(img.getOriginalFilename());
//			System.out.println(path);
			
			
			
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		contact.setLink(img.getOriginalFilename());
		contactService.save(contact);
		redirect.addFlashAttribute("successMessage", "Saved contact successfully!");
		return "redirect:/index";
	}
	
	
	@GetMapping(value = "/contact/{id}/edit")
	public String edit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("contact", contactService.findOne(id));
		return "add-contact";
	}
	
	@GetMapping(value = "/403")
	public String accessDenied() {
		return "403";
	}
	
	
//	@GetMapping("/logout")
//	public String logout(HttpServletRequest request, HttpServletResponse response) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if(auth != null) {
//			new SecurityContextLogoutHandler().logout(request, response, auth);
//		}
//		return "redirect:/";
//	}
	
}
