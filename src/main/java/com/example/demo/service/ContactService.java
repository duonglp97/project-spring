package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Contact;
import com.example.demo.repository.ContactRepository;

@Service
public class ContactService implements IContact{
	
	@Autowired
	private ContactRepository contactRepository;
	@Override
	public Iterable<Contact> findAll() {
		// TODO Auto-generated method stub
		return contactRepository.findAll();
	}

	@Override
	public Optional<Contact> findOne(Integer id) {
		// TODO Auto-generated method stub
		return contactRepository.findById(id);
	}

	@Override
	public List<Contact> search(String name) {
		// TODO Auto-generated method stub
		return contactRepository.findByNameContaining(name);
	}

	@Override
	public void save(Contact contact) {
		// TODO Auto-generated method stub
		contactRepository.save(contact);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		contactRepository.deleteById(id);
		
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return contactRepository.count();
	}

}
