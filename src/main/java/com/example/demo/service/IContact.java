package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.entity.Contact;

public interface IContact {
	Iterable<Contact> findAll();
	Optional<Contact> findOne(Integer id);
	List<Contact> search(String name);
	void save(Contact contact);
	void delete(Integer id);
	long count();
}
