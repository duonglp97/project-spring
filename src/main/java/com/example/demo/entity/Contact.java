package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.ValueGenerationType;

@Entity
@Table(name = "contact")
public class Contact {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contact_id")
	private int id;
	
	@NotEmpty
	@Column(name = "contact_name")
	private String name;
	
	@Email
	@Column(name = "contact_email")
	private String email;
	
	
	@Column(name = "contact_phone")
	private int phone;
	
	@Column(name = "contact_link_img")
	private String link;
	
//	@ManyToOne
//	@JoinColumn(name = "category_id")
//	private Category category;
	
	public Contact() {
		super();
	}
	public Contact(int id, String name, String email, int phone, String link) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.link = link;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
