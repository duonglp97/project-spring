package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Integer> {
	List<Contact> findByNameContaining(String name);
	List<Contact> findByEmailContaining(String email);
	List<Contact> findByPhone(int number);
}
