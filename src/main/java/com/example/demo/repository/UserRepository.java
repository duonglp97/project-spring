package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;



@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
//	boolean existsUser(String name, String password);
//	@Query("SELECT e FROM User e WHERE e.user_name = :name AND e.user_password = :password")
	User findByName(String name);
}