package com.example.demo.config;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		// TODO Auto-generated method stub
		
		if(roleRepository.findByName("ROLE_ADMIN")== null) {
			roleRepository.save(new Role("ROLE_ADMIN"));
		}
		
		if(roleRepository.findByName("ROLE_MEMBER")== null) {
			roleRepository.save(new Role("ROLE_MEMBER"));
		}
		
		
		if(userRepository.findByName("duong")== null) {
			User admin = new User();
			admin.setName("duong");
			admin.setPass(passwordEncoder.encode("123456"));
			HashSet<Role> roles = new HashSet<Role>();
			roles.add(roleRepository.findByName("ROLE_ADMIN"));
			roles.add(roleRepository.findByName("ROLE_MEMBER"));
			admin.setRoles(roles);
			userRepository.save(admin);
			
		}
		if(userRepository.findByName("huy")== null) {
			User member = new User();
			member.setName("huy");
			member.setPass(passwordEncoder.encode("123456"));
			HashSet<Role> roles = new HashSet<Role>();
			
			roles.add(roleRepository.findByName("ROLE_MEMBER"));
			member.setRoles(roles);
			userRepository.save(member);
			
		}
	}
	
}
